//////////////////////////////////////////////////////////////////////////////
//Con el objeto de ofrecer una mejor separabilidad en la ejecuci�n en paralelo 
//de modelos jer�rquicos, esta funci�n devuelve una tabla con un registro para
//cada combinaci�n activa de par�metros que contiene adem�s informaci�n sobre  
//el �mbito en el que se aplica la combinaci�n, para poder distinguir entre
//
// * combinaciones internas a un bloque de un submodelo concreto, 
// * combinaciones que afectan a par�metros de distintos bloques de un
//   mismo submodelo,
// * combinaciones que afectan a par�metros de distintos submodelos
//
Set GetActiveCombinationsInfo(MMS::@Model mms)
//////////////////////////////////////////////////////////////////////////////
{
  Set combinations = mms::GetMCombinations(?);
  SetConcat(For(1,Card(combinations),Set(Real c)
  {
    NameBlock comb = combinations[c];
    Real isActive = comb::IsActive(?);
    If(!isActive, Copy(Empty),{
      Set parameters = comb::GetParameters(?);
      Set prm.rng = Range(1,Card(parameters),1);
      Set prm.subMod = EvalSet(prm.rng,Text(Real k)
      {
        NameBlock prm = parameters[k];
        prm::GetParent(?)::GetParent(?)::GetName(?)
      });
      Set comb.subMod = 
      {
        Set aux = Classify(prm.rng,Real(Real a, Real b)
        {
          Compare(prm.subMod[a],prm.subMod[b])
        });
        EvalSet(aux,Text(Set cls)
        {
          prm.subMod[ cls[1] ]
        })
      };
      Set prm.subClass = EvalSet(prm.rng,Text(Real k)
      {
        NameBlock prm = parameters[k];
        prm::GetParent(?)::GetSubclass(?)
      });
      Set comb.subClass = 
      {
        Set aux = Classify(prm.subClass,Compare);
        EvalSet(aux,Text(Set cls) { cls[1] })
      };
      Set prm.isAdditive = EvalSet(prm.rng,Real(Real k)
      {
        NameBlock prm = parameters[k];
        NameBlock parent = prm::GetParent(?);
        If(!FunctionExist("Real","parent::IsAdditive"),-1,
          parent::IsAdditive(?))
      });
      Set comb.isAdditive = 
      {
        Set aux = Classify(prm.isAdditive,Compare);
        EvalSet(aux,Real(Set cls) { cls[1] })
      };
      [[ [[
         @NameBlock comb = [[ combinations[c] ]],
         //Identificaci�n de submodelo de los par�metros
         // * Si hay un s�lo submodelo implicado se toman su nombre y su 
         //   �ndice
         // * Si hay m�s de uno de deja la cadena vac�a como nombre el 
         //   �ndice se toma a cero
         Text subMod.name = If(Card(comb.subMod)>1,"",
           comb.subMod[1]);
         Real subMod.idx = If(Card(comb.subMod)>1,0,
           mms::FindSubmodel(comb.subMod[1]));
         //Identificaci�n de subclase de los par�metros
         // * Si todos los par�metros son de la misma sub-clase se toma su
         //   nombre 
         // * Si hay m�s de una se toma la cadena vac�a
         Text subClass = If(Card(comb.subClass)>1,"",comb.subClass[1]);
         //Identificaci�n de aditividad de los par�metros
         // * Si todos los par�metros son aditivos se toma 1
         // * Si todos los par�metros son no aditivos se toma 0
         // * Si hay par�metros aditivos y no aditivos se toma ?
         Real isAdditive = If(Card(comb.isAdditive)>1,?,comb.isAdditive[1])
      ]] ]]
    })
  }))
};

//////////////////////////////////////////////////////////////////////////////
//A�ade una combinaci�n MMS a un m�dulo MPM
Real AddComb(MPM::@Module module, MMS::@MCombination comb) 
//////////////////////////////////////////////////////////////////////////////
{ 
  Text name = comb::GetIdentifier(?);
  WriteLn("[Mms2Mpm::AddComb] Adding linear combination "+name+
    " specific for module "<<module::_.name);
  Set parametersId =  comb::GetParameters.Identifier(?);
  Set coefficients = comb::GetCoefficients(?);
  Real activePrior = If(Not(comb::HasPrior(?)),False,
    comb::GetPrior(?)::IsActive(?));
  Real activeCnstr = If(Not(comb::HasConstraint(?)),False,
    comb::GetConstraint(?)::IsActive(?));
  Real nu = If(!activePrior,?,comb::GetPrior.Mean(?));
  Real sigma = If(!activePrior,?,comb::GetPrior.Sigma(?));
  Real lower = If(!activeCnstr,-1/0,comb::GetConstraint.InferiorValue(?));
  Real upper = If(!activeCnstr,+1/0,comb::GetConstraint.SuperiorValue(?));
  If(Or(IsFinite(lower),IsFinite(upper)),
    module::addCombBounds(name, parametersId, coefficients, lower, upper));
  If(IsFinite(sigma),
    module::addCombNormalPrior(name, parametersId, coefficients, nu, sigma)); 
  True
};

//////////////////////////////////////////////////////////////////////////////
//A�ade a un m�dulo MPM un conjunto de combinaciones como el devuelto por 
//GetActiveCombinationsInfo
Real AddCombSet(MPM::@Module module, Set combSet) 
//////////////////////////////////////////////////////////////////////////////
{
  WriteLn("[Mms2Mpm::AddCombSet] Adding "<<Card(combSet)+
    " linear combinations to module "<<module::_.name);
  SetMin(EvalSet(combSet,Real(Set ci)
  {
    AddComb(module, $(ci::comb))
  }))
};

//////////////////////////////////////////////////////////////////////////////
//Crea un m�dulo de combinaci�n externa en un modelo jer�rquico sobre el que 
//aplicar un conjunto de combinaciones que afectan a m�s de un submodelo.
//Cuando los par�metros de los submodelos se modifiquen, autom�ticamente se
//actualizar�n los valores en el m�dulo de combinaci�n externa.
Real BuildExternCombSetModule(MPM::@Model mpm, Set combSet) 
//////////////////////////////////////////////////////////////////////////////
{
  If(!Card(combSet),False,
  {
    WriteLn("[Mms2Mpm::BuildCombSetModule] Building module for external "
      "linear combinations");
    Set externParam = SetConcat(EvalSet(combSet, Set(Set ci)
    {
      MMS::@MCombination comb = $(ci::comb);
      comb::GetParameters.Identifier(?)
    }));
    @NameBlock elc = MPM::@ModuleExternalLinearComb::NewRef(
      mpm, mpm::_.name+".LinealComb", externParam);
    Real mpm::addModule($elc);
    Real AddCombSet($elc,combSet);
    True 
  })
};
